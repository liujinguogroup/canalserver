package com.example.ljg.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Account extends ParamsBase {

    private String id;
    @JsonProperty(value = "user_id", required = true)
    private String user_id;
    private String money;

}
