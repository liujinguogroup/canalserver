package com.example.ljg.models;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ParamsBase {

    public Map<String,Object> getParams(){
        Map<String,Object> map = new HashMap<>();
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                map.put(field.getName(),field.get(this));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return map;
    }
}
