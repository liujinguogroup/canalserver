package com.example.ljg.models;

import lombok.Data;

import java.io.Serializable;

@Data
public class Storage  extends ParamsBase implements Serializable {

    private String id;
    private String commodity_code;
    private String goods_name;
    private String goods_type;
    private String count;
}
