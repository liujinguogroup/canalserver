package com.example.ljg.annotation;

import com.example.ljg.enums.BusinessType;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataHandler {

    BusinessType value();
}
