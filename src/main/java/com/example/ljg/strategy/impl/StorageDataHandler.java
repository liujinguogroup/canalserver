package com.example.ljg.strategy.impl;

import com.alibaba.otter.canal.protocol.CanalEntry;

import com.example.ljg.models.Storage;
import com.example.ljg.strategy.DataHandlerAbstract;
import com.example.ljg.strategy.DataHandlerStrategy;
import com.example.ljg.utils.ObjectConvertUtil;
import lombok.extern.log4j.Log4j2;
 import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Log4j2
@Component("storage_tbl")
public class StorageDataHandler extends DataHandlerAbstract implements DataHandlerStrategy {

    private static final String Storage_prefix = "storage_";

    @Autowired
    private RedisTemplate<String, Object> stringSerializerRedisTemplate;

    @Override
    public void handlerData(List<CanalEntry.Column> afterColumnsList) {
        log.info("处理库存数据");
        Storage storage = new Storage();
        Map<String, Object> map = super.columnTransferMap(storage.getParams(), afterColumnsList);
        try {
            Storage storage1 = ObjectConvertUtil.mapToObj(map, storage.getClass());
            // 得到需要同步的数据
            // 选择同步至 Redis/ElasticSearch/Mysql
            stringSerializerRedisTemplate.opsForValue().set(Storage_prefix + storage1.getCommodity_code(),storage1.getCount());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
