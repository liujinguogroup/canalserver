package com.example.ljg.strategy.impl;

import com.alibaba.otter.canal.protocol.CanalEntry;
import com.example.ljg.models.Account;
import com.example.ljg.strategy.DataHandlerAbstract;
import com.example.ljg.strategy.DataHandlerStrategy;
import com.example.ljg.utils.ObjectConvertUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Log4j2
@Component(value = "order_tbl")
public class OrderDataHandler extends DataHandlerAbstract implements DataHandlerStrategy {

    @Override
    public void handlerData(List<CanalEntry.Column> afterColumnsList) {
        log.info("处理账户数据");
        Account account = new Account();
        Map<String, Object> map = super.columnTransferMap(account.getParams(), afterColumnsList);
        try {
            Account account1 = ObjectConvertUtil.mapToObj(map, account.getClass());

            log.info("account实体：{}",account1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
