package com.example.ljg.strategy;

import com.alibaba.otter.canal.protocol.CanalEntry;
import lombok.extern.log4j.Log4j2;

import java.util.List;
import java.util.Map;

@Log4j2
public abstract class DataHandlerAbstract {

    public Map<String, Object> columnTransferMap(Map<String, Object> params, List<CanalEntry.Column> afterColumnsList) {
        afterColumnsList.forEach(column -> {
            if (params.get(column.getName()) == null){
                params.put(column.getName(),column.getValue());
            }
            log.info("当前字段名： {},字段值：{}",column.getName(),column.getValue());
        });
        return params;
    }



}
