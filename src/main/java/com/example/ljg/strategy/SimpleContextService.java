package com.example.ljg.strategy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class SimpleContextService {

    @Autowired
    private final Map<String,DataHandlerStrategy> strategyMap = new ConcurrentHashMap<>();

    public SimpleContextService(Map<String, DataHandlerStrategy > strategyMap) {
        this.strategyMap.clear();
        strategyMap.forEach(strategyMap::put);
    }

    public DataHandlerStrategy getResource(String poolId){
        return strategyMap.get(poolId);
    }


}
