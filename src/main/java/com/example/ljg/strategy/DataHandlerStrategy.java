package com.example.ljg.strategy;

import com.alibaba.otter.canal.protocol.CanalEntry;

import java.util.List;

public interface DataHandlerStrategy {

    void handlerData(List<CanalEntry.Column> afterColumnsList);
}
