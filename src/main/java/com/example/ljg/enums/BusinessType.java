package com.example.ljg.enums;


import lombok.Getter;

@Getter
public enum BusinessType {

    Account("account_tbl","com.example.ljg.entity.Account"),
    Storage("storage_tbl","com.example.ljg.entity.Storage");

    BusinessType(String dataName,String classPath) {
        DataName = dataName;
        ClassPath = classPath;
    }

    private String DataName;
    private String ClassPath;


}
